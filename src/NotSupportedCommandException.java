public class NotSupportedCommandException extends Exception {
    public NotSupportedCommandException() {
        super("This command is not supported for Robot");
    }
}
