public class RobotControl {
    public static final Position INITIAL_POSITION = Position.BOTTOM;

    Position position;

    public RobotControl() {
        position = INITIAL_POSITION;
    }

    public RobotControl(Position position) {
        this.position = position;
    }

    public void move(char command) throws NotSupportedCommandException {
        if (command == 'W') {
            if(!isOnTop()) {
                moveTop();
            }
            return;
        }

        if (command == 'S') {
            if(!isOnBottom()) {
                moveBottom();
            }
            return;
        }

        throw new NotSupportedCommandException();
    }

    public void moveBottom() {
        position = Position.BOTTOM;
    }

    public void moveTop() {
        position = Position.TOP;
    }

    private boolean isOnBottom() {
        return position == Position.BOTTOM;
    }

    private boolean isOnTop() {
        return position == Position.TOP;
    }
}
