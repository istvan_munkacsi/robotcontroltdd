import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class RobotControlTest {

    @Test
    public void isInitialPositionCorrect() {
        final RobotControl robotControl = new RobotControl();
        assert robotControl.position == RobotControl.INITIAL_POSITION;
    }

    @Test
    public void moveForward() throws NotSupportedCommandException {
        final RobotControl robotControl = new RobotControl(Position.BOTTOM);
        robotControl.move('W');
        assert robotControl.position == Position.TOP;
    }

    @Test
    public void moveBackward() throws NotSupportedCommandException {
        final RobotControl robotControl = new RobotControl(Position.TOP);
        robotControl.move('S');
        assert robotControl.position == Position.BOTTOM;
    }

    @Test
    public void unsupportedCommand() {
        final RobotControl robotControl = new RobotControl();
        Assertions.assertThrows(NotSupportedCommandException.class, () -> robotControl.move('B'));
    }

    @Test
    public void sameStateNotReset() throws NotSupportedCommandException {
        final RobotControl robotControl = Mockito.mock(RobotControl.class);

        robotControl.move('S');
        robotControl.move('S');
        robotControl.move('S');
        Mockito.verify(robotControl, Mockito.atMostOnce()).moveBottom();

        robotControl.move('W');
        robotControl.move('W');
        robotControl.move('W');
        Mockito.verify(robotControl, Mockito.atMostOnce()).moveTop();
    }
}